import React, { Component } from 'react';
import './App.css';
import { Table, Input, Button } from 'antd';

const columns = [
  {
    title: '할일',
    dataIndex: 'TODO',
    align: 'center',
    width: '35%',
  },
  {
    title: '작성자',
    dataIndex: 'USER_NAME',
    align: 'center',
    width: '10%',
  },
  {
    title: '작성일',
    dataIndex: 'REG_DTTM',
    align: 'center',
    width: '20%',
  },
  {
    title: '했니?',
    dataIndex: 'Y_N',
    align: 'center',
    width: '10%',
  },
];

let idx = 0;

const aa = () => {
  return 1;
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoObj: {},
      list: [
        {
          TODO: (
            <Input
              value={(this.state && this.state.TODO) || ''}
              onChange={(e) => this.handleOnchange('TODO', e.target.value)}
            />
          ),
          USER_NAME: (
            <Input
              value={(this.state && this.state.USER_NAME) || ''}
              onChange={(e) => this.handleOnchange('USER_NAME', e.target.value)}
            />
          ),
          REG_DTTM: (
            <Input
              value={(this.state && this.state.REG_DTTM) || ''}
              onChange={(e) => this.handleOnchange('REG_DTTM', e.target.value)}
            />
          ),
          Y_N: (
            <Input
              value={(this.state && this.state.Y_N) || ''}
              onChange={(e) => this.handleOnchange('Y_N', e.target.value)}
              onPressEnter={() => {
                const { todoObj, list } = this.state;
                this.setState({
                  list: list.concat({ ...todoObj, INDEX: idx++ }),
                });
              }}
            />
          ),
          INDEX: 0,
        },
        {
          TODO: '노트북 사기',
          UESR_NAME: '이인섭',
          REG_DTTM: '2020-06-13',
          Y_N: 'O',
          INDEX: idx++,
        },
        {
          TODO: '노트북 사기',
          UESR_NAME: '이인섭',
          REG_DTTM: '2020-06-14',
          Y_N: 'O',
          INDEX: idx++,
        },
        {
          TODO: '노트북 사기',
          UESR_NAME: '이인섭',
          REG_DTTM: '2020-06-15',
          Y_N: 'O',
          INDEX: idx++,
        },
        {
          TODO: '노트북 사기',
          UESR_NAME: '이인섭',
          REG_DTTM: '2020-06-16',
          Y_N: 'O',
          INDEX: idx++,
        },
        {
          TODO: '노트북 사기',
          UESR_NAME: '이인섭',
          REG_DTTM: '2020-06-17',
          Y_N: 'O',
          INDEX: idx++,
        },
        {
          TODO: '노트북 사기',
          UESR_NAME: '이인섭',
          REG_DTTM: '2020-06-18',
          Y_N: 'O',
          INDEX: idx++,
        },
      ],
    };
  }

  handleOnchange = (target, value) => {
    const { todoObj } = this.state;

    return this.setState({
      todoObj: { ...todoObj, [target]: value },
    });
  };

  handleTodoAdd = () => {
    const { todoObj, list } = this.state;

    this.setState({
      list: list.concat({ ...todoObj, INDEX: idx++ }),
      todoObj: { INDEX: idx++ },
    });
  };

  render() {
    const { list } = this.state;
    return (
      <div className="App">
        <Button
          onClick={() => {
            const { todoObj, list } = this.state;
            const listLen = list.length;
            this.setState({
              list: list.concat({ ...todoObj, INDEX: listLen + 3 }),
            });
          }}
        >
          추가
        </Button>
        <Table columns={columns} dataSource={list} rowKey="INDEX"></Table>
      </div>
    );
  }
}

export default App;
